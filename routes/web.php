<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->post('/register','UsersController@register');

$router->group([
    'middleware' => 'auth:api'
], function (\Laravel\Lumen\Routing\Router $router) {
      $router->group(['prefix' => '/v1/funding'], function () use ($router) {
          $router->get('factoring/', 'FundingController@factoring');
          $router->get('factoring/{id}/payouts', 'FundingController@factoringPayouts');
      });
/*
    $router->get('/test-password', function () {
        return 'it worked';
    });
*/
});

/*

*/
